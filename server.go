package main

import (
	"fmt"
	"io"
	"math/rand"
	"net/http"
	"time"
)

type responseWriter struct {
	http.ResponseWriter
	status int
}

var statuses = []int{http.StatusOK, http.StatusAccepted, http.StatusUnauthorized, http.StatusBadRequest, http.StatusBadGateway}
var sleepDurations = []time.Duration{time.Duration(0), time.Second, time.Second * 10, time.Second * 30}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func (rw *responseWriter) WriteHeader(code int) {
	rw.status = code
	rw.ResponseWriter.WriteHeader(code)
}

func logger(next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fields := map[string]interface{}{
			"req_headers": r.Header,
			"req_host":    r.Host,
			"req_uri":     r.RequestURI,
			"req_addr":    r.RemoteAddr,
		}
		fmt.Printf("received request: %+v\n", fields)

		ww := &responseWriter{ResponseWriter: w}

		next.ServeHTTP(ww, r)

		fields["res_status"] = ww.status
		fmt.Printf("served request: %+v\n", fields)
	}
}

func listenAndServe(addr string, random bool) error {
	m := http.NewServeMux()

	m.HandleFunc("/", logger(handler(random)))

	fmt.Printf("listening on: %s\n", addr)
	return http.ListenAndServe(addr, m)
}

func handler(random bool) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if r.Header.Get("Authorization") != "gitlab-registry-notification" {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		b, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			w.Write([]byte(`something went wrong: ` + err.Error()))
			return
		}

		fmt.Printf("%s\n", b)

		// random sleep to simulate slow responses
		time.Sleep(randomSleep(random))
		w.WriteHeader(randomStatus(random))
	}
}

func randomStatus(random bool) int {
	if !random {
		return http.StatusOK
	}
	return statuses[rand.Intn(len(statuses)-1)]
}

func randomSleep(random bool) time.Duration {
	if !random {
		return 0
	}
	return sleepDurations[rand.Intn(len(statuses)-1)]
}
