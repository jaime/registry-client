package main

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"net/http"
	"net/http/httputil"
	"net/url"
	"os"
	"path"
	"regexp"
	"strings"
	"time"

	"github.com/opencontainers/go-digest"
)

const (
	headerWwwAuthenticate     = "Www-Authenticate"
	headerDockerContentDigest = "Docker-Content-Digest"
)

var (
	errUnauthorized = errors.New("unauthorized")
	errNotFound     = errors.New("not found")
	errUnknown      = errors.New("something went wrong")
	errNoAuthHeader = errors.New("no Www-Authenticate header in response")

	regexRealmService          = regexp.MustCompile("realm=\"(.*)\",service=\"(.*)\"")
	regexRealmServiceWithScope = regexp.MustCompile("realm=\"(.*)\",service=\"(.*)\",scope=\"(.*)\",error")
)

func main() {
	serverURL := flag.String("server-url", "https://registry.gitlab.com", "server url e.g. https://registry.gitlab.com")
	apiVersion := flag.String("api-version", "/v2/", "API base path and version e.g. /v2/")
	project := flag.String("project", "", "project name")
	tag := flag.String("tag", "latest", "Image tag or digest")
	token := flag.String("token", "", "Personal Access Token https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html")
	username := flag.String("username", "", "GitLab instance username owner of the token")
	debug := flag.Bool("debug", false, "Print debug information")
	push := flag.Bool("push", false, "push project with tag from serverURL")
	pull := flag.Bool("pull", false, "pull project with tag from serverURL")
	manifestList := flag.Bool("manifest-list", false, "GET /v2/<name>/manifests/<reference>")
	shaFromFile := flag.String("sha-from-file", "", "get the sha256 from file input")
	bearer := flag.Bool("bearer", false, "gets a valid bearer token with pull and push scope")
	bearerScope := flag.String("scope", "", "specify scope for bearer token")
	serveAdrr := flag.String("serve-addr", "", "listen on serve-addr")
	random := flag.Bool("random", false, "add randomness to response status")
	//delete:= flag.Bool("delete-manifest", false, "delete manifest by digest")
	flag.Parse()
	if *shaFromFile != "" {
		fmt.Println(getFileSha(*shaFromFile))
		return
	}

	c := &client{
		c: &http.Client{
			Timeout: 10 * time.Second,
			//Transport: &http2.Transport{},
		},
		serverURL:           *serverURL,
		basePath:            *apiVersion,
		project:             *project,
		tag:                 *tag,
		username:            *username,
		personalAccessToken: *token,
		debug:               *debug,
		overrideScope:       *bearerScope,
	}
	if c.debug {
		fmt.Printf("client with config: %+v\n", c)
	}

	if *serveAdrr != "" {
		if err := listenAndServe(*serveAdrr, *random); err != nil {
			panic(err)
		}

		return
	}

	// https://gitlab.com/gitlab-org/container-registry/-/blob/master/docs-gitlab/push-pull-request-flow.md#pull
	// first step is GET /v2/
	err := c.V2()
	if err != nil {
		fmt.Printf("GET V2: %+v\n", err)
		os.Exit(1)
	}

	fmt.Println("GET /v2/ succedeed!")
	if *bearer {
		fmt.Println(c.currentBearerToken)
		return
	}

	if *pull {
		c.pull()
	} else if *push {
		c.push()
	} else if *manifestList {
		c.getManifestList()
	} else {
		fmt.Printf("missing operation: either --push or --pull")
		os.Exit(1)
	}
}
func getFileSha(filename string) string {
	f, err := os.Open(filename)
	if err != nil {
		fatalf(fmt.Sprintf("open file: %q err: %+v", filename, err))
	}
	fi, err := f.Stat()
	if err != nil {
		fatalf(fmt.Sprintf("fileinfo: %q err: %+v", filename, err))
	}

	fmt.Printf("%q - %d\n", fi.Name(), fi.Size())

	dgst, err := digest.FromReader(f)
	if err != nil {
		fatalf(fmt.Sprintf("digest.FromReader: %q err: %+v", filename, err))
	}

	return dgst.String()
}
func (c *client) getManifestList() {
	manifest, err := c.Manifest(true)
	if err != nil {
		fatalf(fmt.Sprintf("failed to decode GET manifest: %q - %+v", c.tag, err))
	}

	fmt.Printf("the manifest: \n%+v\n", manifest)

	//buf := bytes.Buffer{}
	//b, err := json.Marshal(manifest)
	b, err := json.MarshalIndent(manifest, "", "  ")
	if err != nil {
		fatalf(fmt.Sprintf("failed to decode manifest list struct: %q - %+v", c.tag, err))
	}
	// add missing new line
	b = append(b, '\n')
	fmt.Printf("%s\n", b)
	fmt.Printf("size: %d\n", len(b))
	sha256 := digest.FromBytes(b)
	fmt.Printf("got sha: %q\nDocker-Content-Digest: %q\n equal? %t\n\n", sha256, manifest.dockerContentDigest, sha256.String() == manifest.dockerContentDigest)

}

type client struct {
	c                   *http.Client
	serverURL           string
	basePath            string
	project             string
	tag                 string
	username            string
	personalAccessToken string
	currentBearerToken  string
	debug               bool
	overrideScope       string
}

func (c *client) reqWithAuth(method, path string, body io.Reader) (*http.Response, error) {
	header := http.Header{}
	if c.currentBearerToken != "" {
		header.Set("Authorization", "Bearer "+c.currentBearerToken)
	}

	return c.reqWithHeaders(method, path, body, header)
}

func (c *client) reqWithAuthAndHeaders(method, path string, body io.Reader, header http.Header) (*http.Response, error) {
	if c.currentBearerToken != "" {
		header.Set("Authorization", "Bearer "+c.currentBearerToken)
	}

	return c.reqWithHeaders(method, path, body, header)
}

func (c *client) reqWithHeaders(method, path string, body io.Reader, header http.Header) (*http.Response, error) {
	u, err := url.Parse(c.serverURL + c.basePath + path)
	if err != nil {
		return nil, fmt.Errorf("parse url: %w", err)
	}

	res, err := c.req(method, u.String(), header, body)
	if err != nil {
		return nil, fmt.Errorf("reqWithAuth response: %w", err)
	}

	return res, nil
}

func (c *client) V2() error {
	// empty path -> /v2/
	res, err := c.reqWithAuth(http.MethodGet, "", nil)
	if err != nil {
		return err
	}
	resHeader := res.Header.Clone()
	status := res.StatusCode
	// read header and close response
	//res.Body.Close()

	switch status {
	case http.StatusUnauthorized:
		err := c.getOAuthToken(resHeader)
		if err != nil {
			return err
		}

		// retry /v2/
		res, err := c.reqWithAuth(http.MethodGet, "", nil)
		if err != nil {
			return err
		}

		if res.StatusCode != http.StatusOK {
			return errUnauthorized
		}

		return nil
	case http.StatusNotFound:
		return errNotFound
	default:
		if status >= http.StatusInternalServerError {
			return errUnknown
		}

		fmt.Printf("status: %d\n", status)
		return nil
	}
}

func (c *client) getOAuthToken(header http.Header) error {
	realm, service, scope, err := parseWwwAuthHeader(header)
	if err != nil {
		return err
	}

	u, err := url.Parse(realm)
	if err != nil {
		return err
	}
	q := u.Query()
	if service != "" {
		q.Add("service", service)
	}

	if scope == "" {
		scope = fmt.Sprintf("repository:%s:pull,push", c.project)
	}

	if c.overrideScope != "" {
		scope = c.overrideScope
	}

	q.Add("scope", scope)

	u.RawQuery = q.Encode()
	if c.debug {
		fmt.Printf("getting OAuth token from: %q\n", u.String())
	}

	newHeader := http.Header{}

	newHeader.Set("Authorization", "Basic "+basicAuth(c.username, c.personalAccessToken))
	res, err := c.req(http.MethodGet, u.String(), newHeader, nil)
	if err != nil {
		return err
	}
	defer res.Body.Close()

	token := struct {
		Token string `json:"token"`
	}{}

	if err := json.NewDecoder(res.Body).Decode(&token); err != nil {
		return fmt.Errorf("decode token response: %w", err)
	}

	// set bearer token
	c.currentBearerToken = token.Token

	if c.debug {
		fmt.Printf("the token: %q\n", c.currentBearerToken)
	}
	return nil
}

// get the realm's URL and the service needed to request the OAuth token
// e.g. Www-Authenticate: Bearer realm="https://gitlab.com/jwt/auth",service="container_registry",scope="repository:jaime/test-package-private:pull",error="insufficient_scope"
func parseWwwAuthHeader(header http.Header) (string, string, string, error) {
	authHeader := header.Get(headerWwwAuthenticate)
	if authHeader == "" {
		return "", "", "", errNoAuthHeader
	}
	matches := regexRealmServiceWithScope.FindStringSubmatch(authHeader)
	if len(matches) == 0 {
		// try without scope
		matches = regexRealmService.FindStringSubmatch(authHeader)
	}

	if len(matches) == 3 {
		// no scope in header
		return matches[1], matches[2], "", nil
	} else if len(matches) == 4 {
		return matches[1], matches[2], matches[3], nil
	}

	return "", "", "", errNoAuthHeader
}

func basicAuth(username, password string) string {
	auth := username + ":" + password
	return base64.StdEncoding.EncodeToString([]byte(auth))
}

func (c *client) req(method, url string, extraHeaders http.Header, body io.Reader) (*http.Response, error) {
	req, err := http.NewRequest(method, url, body)
	if err != nil {
		return nil, fmt.Errorf("new request: %w", err)
	}

	for k, v := range extraHeaders {
		req.Header.Set(k, strings.Join(v, ","))
	}

	if c.debug {
		dreq, err := httputil.DumpRequestOut(req, true)
		fmt.Printf("req:\n%s\n%+v\n", dreq, err)
	}

	res, err := c.c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("do: %w", err)
	}

	if c.debug {
		// don't print response body for blob requests
		printBody := !strings.Contains(url, "blobs")
		dres, err := httputil.DumpResponse(res, printBody)
		fmt.Printf("res:\n%s\n%+v\n", dres, err)
	}

	return res, nil
}

// Descriptor as defined by OCI https://github.com/opencontainers/image-spec/blob/main/descriptor.md#properties
type Descriptor struct {
	MediaType   string            `json:"mediaType"`
	Size        int               `json:"size"`
	Digest      string            `json:"digest"`
	URLs        []string          `json:"urls,omitempty"`
	Annotations map[string]string `json:"annotations,omitempty"`
	Platform    struct {
		Architecture string `json:"architecture"`
		Os           string `json:"os"`
	} `json:"platform,omitempty"`
}

// Manifest for an image https://github.com/opencontainers/image-spec/blob/main/manifest.md
type Manifest struct {
	SchemaVersion       int               `json:"schemaVersion"`
	MediaType           string            `json:"mediaType"`
	Config              *Descriptor       `json:"config,omitempty"`
	Manifests           []Descriptor      `json:"manifests,omitempty"`
	Layers              []Descriptor      `json:"layers,omitempty"`
	Annotations         map[string]string `json:"annotations,omitempty"`
	dockerContentDigest string            `json:"-"`
}

func (c *client) Manifest(list bool) (*Manifest, error) {
	fmt.Printf("Downloading manifest for %s:%s\n", c.project, c.tag)

	ref := c.tag
	res, err := c.reqWithAuth(http.MethodHead, c.project+"/manifests/"+ref, nil)
	if err != nil {
		return nil, fmt.Errorf("manifest req: %w", err)
	}
	res.Body.Close()

	digest := res.Header.Get(headerDockerContentDigest)
	if digest == "" {
		digest = c.tag
	}
	ref = digest

	header := http.Header{}
	if list {
		ref = c.tag
		header.Set("Accept", "application/vnd.docker.distribution.manifest.list.v2+json")
	}

	res, err = c.reqWithAuthAndHeaders(http.MethodGet, c.project+"/manifests/"+ref, nil, header)
	if err != nil {
		return nil, fmt.Errorf("manifest req: %w", err)
	}
	defer res.Body.Close()

	switch res.StatusCode {
	case http.StatusUnauthorized:
		err := c.getOAuthToken(res.Header)
		if err != nil {
			return nil, fmt.Errorf("manifest token: %w", err)
		}

		// retry /v2/
		res2, err := c.reqWithAuthAndHeaders(http.MethodGet, c.project+"/manifests/"+ref, nil, header)
		if err != nil {
			return nil, fmt.Errorf("re-try manifest: %w", err)
		}
		return c.decodeManifest(res2)
	case http.StatusNotFound:
		return nil, fmt.Errorf("manifest for: %s:%s - %w", c.project, c.tag, errNotFound)
	}

	return c.decodeManifest(res)
}

func (c *client) decodeManifest(res *http.Response) (*Manifest, error) {
	defer res.Body.Close()
	dir := path.Join("blobs", c.project, c.tag)

	if res.StatusCode != http.StatusOK {
		return nil, errUnknown
	}

	for header, val := range res.Header {
		fmt.Printf("header: %q - %v\n", header, val)
	}
	var buf bytes.Buffer
	tee := io.TeeReader(res.Body, &buf)

	n, err := saveToFile(dir, "manifest.json", tee)
	if err != nil {
		return nil, err
	}

	if c.debug {
		fmt.Printf("read bytes: %d\nContent-Length: %s\n", n, res.Header.Get("Content-Length"))
	}

	manifest := Manifest{}
	if err := json.NewDecoder(&buf).Decode(&manifest); err != nil {
		return nil, fmt.Errorf("decode manifest: %w", err)
	}

	manifest.dockerContentDigest = res.Header.Get(headerDockerContentDigest)
	//sha256 := digest.FromBytes(buf.Bytes())
	//fmt.Printf("got sha: %q\nDocker-Content-Digest: %q\n equal? %t\n\n", sha256, manifest.dockerContentDigest, sha256.String() == manifest.dockerContentDigest)

	return &manifest, nil
}

// Blob makes the request to the registry and saves the content to a project directory
func (c *client) Blob(digest string) error {
	dir := path.Join("blobs", c.project, c.tag)
	fmt.Printf("downloading blob with digest: %s\n", digest)

	res, err := c.reqWithAuth(http.MethodGet, c.project+"/blobs/"+digest, nil)
	if err != nil {
		return fmt.Errorf("blobs req: %w", err)
	}
	defer res.Body.Close()

	n, err := saveToFile(dir, digest, res.Body)
	if err != nil {
		return err
	}

	if c.debug {
		fmt.Printf("read bytes: %d\nContent-Length: %s\n", n, res.Header.Get("Content-Length"))
	}
	// TODO: verify sha256?
	return nil
}

func saveToFile(dir, name string, reader io.Reader) (int64, error) {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		err := os.MkdirAll(dir, 0766)
		if err != nil {
			return 0, fmt.Errorf("create dir: %w", err)
		}
	}

	f, err := os.Create(path.Join(dir, name))
	if err != nil {
		return 0, fmt.Errorf("create file for blob: %w", err)
	}
	defer f.Close()

	n, err := f.ReadFrom(reader)
	if err != nil {
		return 0, fmt.Errorf("read blob content: %w", err)
	}

	return n, nil
}

func fatalf(msg string) {
	fmt.Println(msg)
	os.Exit(1)
}
