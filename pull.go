package main

import (
	"fmt"
	"os"
	"sync"
)

func (c *client) pull() {
	// second step is GET /v2/<name>/manifests/<reference>
	manifest, err := c.Manifest(false)
	if err != nil {
		fmt.Printf("GET manifest: %+v\n", err)
		os.Exit(1)
	}

	fmt.Printf("GET /v2/<name>/manifests/<reference> succedeed!\n")
	if c.debug {
		fmt.Printf("manifest: \n%+v\n", manifest)
	}

	wg := sync.WaitGroup{}
	wg.Add(len(manifest.Layers) + 1)

	go func() {
		defer wg.Done()
		// download the manifest blob first
		if err := c.Blob(manifest.Config.Digest); err != nil {
			fmt.Printf("GET manifest blob with digest: %q failed: %+v\n", manifest.Config.Digest, err)
		}
	}()

	// try download the rest of the blobs for each layer
	for _, layer := range manifest.Layers {
		go func(digest string) {
			defer wg.Done()
			// download each layer's blob
			if err := c.Blob(digest); err != nil {
				fmt.Printf("GET layer blob with digest: %q failed: %+v\n", digest, err)
			}
		}(layer.Digest)
	}

	wg.Wait()
	fmt.Printf("Finished downloading: %s:%s\n", c.project, c.tag)
}
