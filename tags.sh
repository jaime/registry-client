#!/bin/bash

tag_and_push() {
    docker pull alpine:latest
    docker login registry.gitlab.com -u jaime -p $GITLAB_PRIVATE_TOKEN 


    for i in {38..120}
    do
        docker tag alpine registry.gitlab.com/jaime/registry-tests/alpine:$i
        docker push registry.gitlab.com/jaime/registry-tests/alpine:$i &
        sleep 0.2
    done
    wait
}

tag_and_push