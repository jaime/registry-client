# registry-client

Test project demonstrating how to talk to the [container-registry][] to pull and push images.

## Example pull

With a local [container-registry][] running on port `:5000` with authentication using a personal access token

```shell
$ go run main.go  -server-url http://registry.test:5000 -project root/packages -token $GITLAB_TOKEN_LOCAL -username root -tag latest -pull

getOAuthToken u: "http://gdk.test:3000/jwt/auth?service=container_registry"
GET /v2/ succedeed!
Downloading manifest for root/packages:latest
GET /v2/<name>/manifests/<reference> succedeed!
downloading blob with digest: sha256:4d88d9f4727f855029d89f563ad821ef86beb8f37edefa2a4981ec29e29948ee
downloading blob with digest: sha256:41f84bcdb374a33e9603cc371bc5aebbfe23223ee414bea5dcb8385169a5cfb5
downloading blob with digest: sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e
Finished downloading: root/packages:latest

$ ls -la  blobs/root/packages/latest
drwxr--r-- jaime staff 160 B  Wed Oct 27 16:02:02 2021  .
drwxr--r-- jaime staff  96 B  Wed Oct 27 16:02:02 2021  ..
.rw-r--r-- jaime staff  92 B  Wed Oct 27 16:02:02 2021  sha256:41f84bcdb374a33e9603cc371bc5aebbfe23223ee414bea5dcb8385169a5cfb5
.rw-r--r-- jaime staff 818 B  Wed Oct 27 16:02:02 2021  sha256:4d88d9f4727f855029d89f563ad821ef86beb8f37edefa2a4981ec29e29948ee
.rw-r--r-- jaime staff 2.7 MB Wed Oct 27 16:02:02 2021  sha256:a0d0a0d46f8b52473982a3c466318f479767577551a53ffc9074c9fa7035982e
```

Sample

![pull image](img/demo-pull.png)


[container-registry]: https://gitlab.com/gitlab-org/container-registry


## Testing with mitmproxy

To inspect the request using [`mitmproxy`](https://mitmproxy.org/) follow these steps:

1. Install `mitmproxy` and run it locally

    ```shell
    brew install mitmproxy
    mitmproxy --mode reverse:http://registry.test:5000/
    ```

2. Set the following environment variables:

    ```shell
    # do not proxy the request to obtain a token, e.g. the response in the `realm` portion of the Www-Authenticate header.
    export NO_PROXY="gdk.test:3000"
    export HTTP_PROXY="http://0.0.0.0:8080"
    ```

3. Run the program and inspect the requests!

   ![demo-proxy](img/demo-proxy.gif)


## Internal video demos

Requires access to `gitlab-internal` project on GCP.

Pull:

[![Pull demo](img/pull.png)](https://storage.cloud.google.com/jmartinez-registry-local/demos/registry-client/registry-client-pull.mp4)

Push:

[![Push demo](img/push.png)](https://storage.cloud.google.com/jmartinez-registry-local/demos/registry-client/registry-client-push.mp4)
