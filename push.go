package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"path"
)

func (c *client) push() {
	// V2 is done outside
	projectPath := path.Join("blobs", c.project, c.tag)

	f, err := os.Open(projectPath + "/manifest.json")
	if err != nil {
		fatalf(fmt.Sprintf("failed to open manifest: %q/manifest.json %+v", projectPath, err))
	}
	defer f.Close()

	manifest := Manifest{}
	if err := json.NewDecoder(f).Decode(&manifest); err != nil {
		fatalf(fmt.Sprintf("failed to decode manifest: %q/manifest.json %+v", projectPath, err))
	}
	// for each layer of a manifest + the manifest config blob:
	for _, layer := range append(manifest.Layers, *manifest.Config) {
		err := func(layer *Descriptor) error {
			layerFile, err := os.Open(projectPath + "/" + layer.Digest)
			if err != nil {
				return fmt.Errorf("failed to open layer with digest: %s/%s %w", projectPath, layer.Digest, err)
			}
			defer layerFile.Close()

			// HEAD /v2/<name>/blobs/<digest>
			res, err := c.reqWithAuth(http.MethodHead, c.project+"/blobs/"+layer.Digest, nil)
			if err != nil {
				return fmt.Errorf("HEAD digest: %w", err)
			}
			defer res.Body.Close()

			switch res.StatusCode {
			case http.StatusOK:
				fmt.Printf("layer already exists: %s\n", layer.Digest)
				// 200 already exists
				// nothing to DO!
				return nil

			//404 not found
			// try x-repository mounting only if the client knows about it
			// so will definitely skip this step for now
			case http.StatusNotFound:
				fmt.Printf("layer does not exist: %s, starting upload\n", layer.Digest)
				// initiate upload POST /v2/<name>/blobs/uploads/
				initUploadRes, err := c.reqWithAuth(http.MethodPost, c.project+"/blobs/uploads/", nil)
				if err != nil {
					return fmt.Errorf("init upload digest: %w", err)
				}
				defer initUploadRes.Body.Close()

				if initUploadRes.StatusCode != http.StatusAccepted {
					return fmt.Errorf("unexepected response /blobs/uploads/: %s", initUploadRes.Status)
				}
				// PATCH START
				fmt.Printf("Starting upload for digest: %q\n", layer.Digest)

				//PATCH /v2/<name>/blobs/uploads/<uuid>
				patchURL, err := url.Parse(initUploadRes.Header.Get("Location"))
				if err != nil {
					return fmt.Errorf("invalid location to PATCH blob: %w", err)
				}

				patchReq, err := http.NewRequest(http.MethodPatch, patchURL.String(), layerFile)
				if err != nil {
					return fmt.Errorf("PATCH req: %w", err)
				}
				patchReq.Header.Set("Authorization", "Bearer "+c.currentBearerToken)

				patchRes, err := c.c.Do(patchReq)
				if err != nil {
					return fmt.Errorf("PATCH Do: %w", err)
				}
				defer patchRes.Body.Close()

				if patchRes.StatusCode != http.StatusAccepted {
					return fmt.Errorf("PATCH got unknown response: %s\n", patchRes.Status)
				}

				fmt.Printf("Blob uploaded for digest: %q!\n", layer.Digest)

				/// IF ALL DONE
				// PUT /v2/<name>/blobs/uploads/<uuid>?digest=<digest>
				putURL, err := url.Parse(patchRes.Header.Get("Location"))
				if err != nil {
					return fmt.Errorf("invalid location to PUT blob: %w", err)
				}
				// ensure digest is there
				putQuery := putURL.Query()
				putQuery.Add("digest", layer.Digest)
				putURL.RawQuery = putQuery.Encode()

				putReq, err := http.NewRequest(http.MethodPut, putURL.String(), nil)
				if err != nil {
					return fmt.Errorf("PATCH req: %w", err)
				}
				putReq.Header.Set("Authorization", "Bearer "+c.currentBearerToken)

				putRes, err := c.c.Do(putReq)
				if err != nil {
					return fmt.Errorf("PUT Do: %w", err)
				}
				defer putRes.Body.Close()

				if putRes.StatusCode != http.StatusCreated {
					return fmt.Errorf("PUT got unknown response: %s\n", patchRes.Status)
				}

				fmt.Printf("Blob commited for digest: %q!\n", layer.Digest)
			}
			// Ensure upload succeeded HEAD /v2/<name>/blobs/<digest>
			headRes, err := c.reqWithAuth(http.MethodHead, c.project+"/blobs/"+layer.Digest, nil)
			if err != nil {
				return fmt.Errorf("ensure HEAD reqWithAuth: %w", err)
			}
			switch headRes.StatusCode {
			case http.StatusTemporaryRedirect:
				redirectHeadRes, err := c.req(http.MethodHead, headRes.Header.Get("Location"), nil, nil)
				if err != nil {
					return fmt.Errorf("redirectHead req: %w", err)
				}
				if redirectHeadRes.StatusCode != http.StatusOK {
					return fmt.Errorf("HEAD (ensure upload redirect) got unknown response: %s\n", headRes.Status)
				}

				fmt.Printf("layer with digest %s uploaded correctly (after redirect)!\n", layer.Digest)
				return nil
			case http.StatusOK:
				fmt.Printf("layer with digest %s uploaded correctly!\n", layer.Digest)

				return nil
			default:
				return fmt.Errorf("HEAD (ensure upload) got unknown response: %s\n", headRes.Status)
			}
		}(&layer)
		if err != nil {
			fmt.Printf("failed to upload layer with digest: %q - %+v\n ", layer.Digest, err)
		}
	}

	// ALL LAYERS DONE! Commit manifest now
	//PUT /v2/<name>/manifests/<reference>
	b, err := json.Marshal(manifest)
	if err != nil {
		fatalf(fmt.Sprintf("encoding JSON manifest failed: %s", err))
	}

	putManifestResp, err := c.reqWithAuth(http.MethodPut, c.project+"/manifests/"+c.tag, bytes.NewReader(b))
	if err != nil {
		fatalf(fmt.Sprintf("putManifestResp PUT reqWithAuth: %s", err))
	}

	if putManifestResp.StatusCode != http.StatusCreated {
		fatalf(fmt.Sprintf("PUT manifest failed: %s", putManifestResp.Status))
	}

	fmt.Println("manifest uploaded!")
}
